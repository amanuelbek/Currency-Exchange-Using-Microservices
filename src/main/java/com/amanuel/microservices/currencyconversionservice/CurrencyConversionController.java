package com.amanuel.microservices.currencyconversionservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@RestController
public class CurrencyConversionController {


    @Value("${server.port}")
    private int port;

    @Autowired
    CurrencyExchangeServiceProxy exchangeProxy;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping(value = "/currency-converter/from/{from}/to/{to}/quantity/{quantity}")
    public CurrencyConversionBean convertCurrency(@PathVariable String from,
                                                  @PathVariable String to,
                                                  @PathVariable BigDecimal quantity) {


//        --The hustle of calling a simple MicroService.

//        Map<String, String> uriVariables = new HashMap<>();
//        uriVariables.put("from", from);
//        uriVariables.put("to", to);
//
//        ResponseEntity<CurrencyConversionBean> responseEntity = new RestTemplate().getForEntity("http://localhost:8000/currency-exchange/from/{from}/to/{to}",
//                CurrencyConversionBean.class, uriVariables);
//
//        CurrencyConversionBean response = responseEntity.getBody();
//
//
//        return new CurrencyConversionBean(response.getId(), from, to, response.getConversionMultiple(),
//                quantity, quantity.multiply(response.getConversionMultiple()), response.getPort());


        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("from", from);
        uriVariables.put("to", to);

        ResponseEntity<CurrencyConversionBean> responseEntity = new RestTemplate().getForEntity("http://localhost:8080/currency-exchange/from/"+from+"/to/"+to,
                CurrencyConversionBean.class, uriVariables);
        CurrencyConversionBean response = responseEntity.getBody();

        /**
         * ======================Retrieve a plain JSON=====================
         *

         RestTemplate restTemplate = new RestTemplate();
        String fooResourceUrl
                = "http://localhost:8080/spring-rest/foos";

        ResponseEntity<String> response
                = restTemplate.getForEntity(fooResourceUrl + "/1", String.class);

         * Check the status code of the return
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));


         ObjectMapper mapper = new ObjectMapper();
         JsonNode root = mapper.readTree(response.getBody());
         JsonNode name = root.path("name");
         assertThat(name.asText(), notNullValue());

         **/


        /**
         * =================Retrieve headers using RestTemplate================
         *
         * HttpHeaders httpHeaders = restTemplate
         *   .headForHeaders(fooResourceUrl);
         *
         * assertTrue(httpHeaders.getContentType()
         *   .includes(MediaType.APPLICATION_JSON));
         */

        /**
         * ========== Post an Entity using RestTemplate (postForObject) ======
         *
         *
         * ClientHttpRequestFactory requestFactory = getClientHttpRequestFactory();
         *
         * RestTemplate restTemplate = new RestTemplate(requestFactory);
         *
         * HttpEntity<Foo> request = new HttpEntity<>(new Foo("bar"));
         *
         * Foo foo = restTemplate.postForObject(fooResourceUrl, request, Foo.class);
         *
         *  @Test
         * assertThat(foo, notNullValue());
         * assertThat(foo.getName(), is("bar"));
         *
         *
         *
         *
         */

        /**
         * ========== Post an Entity using Exchange API ======
         *
         *
         * RestTemplate restTemplate = new RestTemplate();
         *
         * HttpEntity<Foo> request = new HttpEntity<>(new Foo("bar"));
         *
         * ResponseEntity<Foo> response = restTemplate
         *   .exchange(fooResourceUrl, HttpMethod.POST, request, Foo.class);
         *
         * @Test
         * assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
         *
         * Foo foo = response.getBody();
         *
         * @Test
         * assertThat(foo, notNullValue());
         * assertThat(foo.getName(), is("bar"));
         *
         */


        /**
         * ======== Post an Entity from a Form ===============
         *
         * HttpHeaders headers = new HttpHeaders();
         * headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
         *
         * MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
         * map.add("id", "1");
         *
         *
         * HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
         *
         * ResponseEntity<String> response = restTemplate.postForEntity(
         *   fooResourceUrl+"/form", request , String.class);
         *
         * @Test
         * assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
         *
         *
         *
         */




        return new CurrencyConversionBean(response.getId(), from, to, response.getConversionMultiple(), quantity,
                quantity.multiply(response.getConversionMultiple()),response.getPort());


    }

    @GetMapping(value = "/currency-converter-feign/from/{from}/to/{to}/quantity/{quantity}")
    public CurrencyConversionBean convertCurrencyFeign(@PathVariable String from,
                                                  @PathVariable String to,
                                                  @PathVariable BigDecimal quantity) {

        CurrencyConversionBean response = exchangeProxy.retrieveExchangeValue(from,to);

        logger.info("{}", response);

        return new CurrencyConversionBean(response.getId(), from, to, response.getConversionMultiple(),
                quantity, quantity.multiply(response.getConversionMultiple()), response.getPort());


    }

}
