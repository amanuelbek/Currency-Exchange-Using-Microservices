package com.amanuel.microservices.currencyconversionservice;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//@FeignClient(name = "currency-exchange-service") --> Feign is not connecting to Zuul API Gateway
//@FeignClient(name = "netflix-zool-api-gateway-server")
//@RibbonClient(name = "currency-exchange-service")
public interface CurrencyExchangeServiceProxy {


//    @GetMapping(value = "/currency-exchange/from/{from}/to/{to}")
    @GetMapping(value = "currency-exchange-service/currency-exchange/from/{from}/to/{to}")
    CurrencyConversionBean retrieveExchangeValue(@PathVariable("from") String from, @PathVariable("to") String to);
}
